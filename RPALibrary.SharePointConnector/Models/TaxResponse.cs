﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPALibrary.SharePointConnector.Models
{
    public class TaxResponse
    {
        public int Id { get; set; }
        public string Tax_Head { get; set; }
        public decimal Amount { get; set; }

    }
}
