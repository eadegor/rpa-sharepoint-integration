﻿using RPALibrary.SharePointConnector.Models;
using System;
using System.Collections.Generic;
using System.Text;
using SP = Microsoft.SharePoint.Client;

namespace RPALibrary.SharePointConnector
{
    
    public class ListManager : IDisposable, IListManager
    {
        AuthenticationManager auth;

        public ListManager(AuthenticationManager auth)
        {
            this.auth = auth;
        }

        public List<TaxResponse> GetTax(RequestList request)
        {
            List<TaxResponse> res = new List<TaxResponse>();

            // var res = new TaxResponse();

            using (var context = auth.GetContext())
            {
                var web = context.Web;
                var list = web.Lists.GetByTitle(request.List);
                // var listFields = list.Fields;
                context.Load(list);
                context.ExecuteQuery();
                var view = list.Views.GetByTitle(request.View);
                context.Load(view);
                context.ExecuteQuery();/**/

                SP.CamlQuery query = new SP.CamlQuery();
                query.ViewXml = view.ViewQuery;

                SP.ListItemCollection items = list.GetItems(query);
                context.Load(items);
                context.ExecuteQuery();

                foreach (SP.ListItem item in items)
                {
                    var r = new TaxResponse
                    {
                        Id = item.Id,
                        Amount = Convert.ToDecimal(item["Amount"]),
                        Tax_Head = item["Tax_Head"].ToString()

                    };

                    res.Add(r);

                }

                return res;

            }
        }

        public void Dispose()
        {
            auth.Dispose();
        }
    }
}
