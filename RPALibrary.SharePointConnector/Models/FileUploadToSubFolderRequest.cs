﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPALibrary.SharePointConnector.Models
{
    public class FileUploadToSubFolderRequest
    {
        public string Library { get; set; }

        public string RootFolder { get; set; }

        public string YearFolder { get; set; }

        public string MonthFolder { get; set; }

        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Properties { get; set; }

        public string GetFileNameWithExtension()
        {
            var ext = FileExtension.StartsWith(".") ? FileExtension : "." + FileExtension;
            return FileName.Replace(" ", "_") + ext;
        }

        public string GetUniqueFileNameWithExtension()
        {
            var ext = FileExtension.StartsWith(".") ? FileExtension : "." + FileExtension;
            // return FileName.Replace(" ", "_").Replace(".", "_") + "-" + Guid.NewGuid().ToString() +  "_"+DateTime.Now.ToString("dd-MM-yyyy")+ ext;
            return FileName.Replace(" ", "_").Replace(".", "_") + "-" + Guid.NewGuid().ToString() + ext;
        }
    }
}
