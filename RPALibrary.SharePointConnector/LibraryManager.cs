﻿using RPALibrary.SharePointConnector.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using SP = Microsoft.SharePoint.Client;

namespace RPALibrary.SharePointConnector
{
    public class LibraryManager : IDisposable, ILibraryManager
    {
        AuthenticationManager auth;
        public LibraryManager(AuthenticationManager auth)
        {
            this.auth = auth;
        }

        private static bool ValidateFieldNames(IEnumerable<string> listFields, IEnumerable<string> propFields, out string invalidField)
        {
            var res = true;
            invalidField = "";
            foreach (var pf in propFields)
            {
                if (!listFields.Any(l => l == pf))
                {
                    res = false;
                    invalidField = pf;
                    break;
                }
            }
            return res;
        }

        public void Dispose()
        {
            auth.Dispose();
        }

        public FileUploadResponse UploadFileToFolder(FileUploadToSubFolderRequest request)
        {
            var res = new FileUploadResponse();
            using (var context = auth.GetContext())
            {
                var web = context.Web;
                var list = web.Lists.GetByTitle(request.Library);

                var listFields = list.Fields;

                context.Load(listFields);
                context.ExecuteQuery();

                var rootFolder = request.RootFolder;
                var yearFolder = request.YearFolder;
                var monthFolder = request.MonthFolder;

                var _listFields = listFields.Where(lf => !lf.Hidden && lf.CanBeDeleted).Select(lf => lf.InternalName).ToList();
                var isFieldsValid = ValidateFieldNames(_listFields, request.Properties.Select(p => p.Key), out string invalidField);
                if (!isFieldsValid)
                    throw new Exception($"Invalid field name '{invalidField}'! Field does not exist or is readonly.");
                else if (!ValidateUploadRequest(request, out string error))
                {
                    throw new Exception(error);
                }

                var _rootFolder = list.RootFolder;
                context.Load(_rootFolder);
                _rootFolder = _rootFolder.Folders.Add(rootFolder);
                var _yearFolder = _rootFolder.Folders.Add(yearFolder);
                var _monthFolder = _yearFolder.Folders.Add(monthFolder);
                context.Load(_monthFolder.Files);
                context.ExecuteQuery();

                var fci = new SP.FileCreationInformation();
                fci.ContentStream = new MemoryStream(request.File);
                fci.Url = request.GetUniqueFileNameWithExtension();
                fci.Overwrite = true;


                var file = _monthFolder.Files.Add(fci);
                context.Load(file);
                file.CheckOut();

                SP.ListItem item = file.ListItemAllFields;
                foreach (var p in request.Properties)
                {
                    item[p.Key] = p.Value;
                }
                item.Update();

                file.CheckIn(string.Empty, SP.CheckinType.OverwriteCheckIn);

                var _item = file.ListItemAllFields;
                context.Load(_item);
                _rootFolder.Update();
                context.ExecuteQuery();
                // return file info
                res.Id = _item.Id;
                res.Guid = file.UniqueId.ToString();
                res.FileName = file.Name;
                res.ServerRelativeUrl = file.ServerRelativeUrl;
                res.AbsoluteUrl = new Uri(context.Url).GetLeftPart(UriPartial.Authority) + file.ServerRelativeUrl;

                return res;
            }
        }

        private bool ValidateUploadRequest(FileUploadToSubFolderRequest request, out string error)
        {
            error = null;
            var _monthYear = request.MonthFolder + " " + request.YearFolder;

            var isValid = DateTime.TryParseExact(_monthYear, "MMMM yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime _date);
            if (isValid)
            {
                return true;
            }
            else
            {
                error = "Invalid Month or Year Value";
                return false;
            }
        }
    }
}
