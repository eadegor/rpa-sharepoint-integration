﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RPALibrary.SharePointConnector;
using RPALibrary.SharePointConnector.Models;

namespace RPALibrary.SharePointAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LibraryController: Controller
    {
        private readonly ILibraryManager libMgr;

        public LibraryController(ILibraryManager libMgr)
        {
            this.libMgr = libMgr;
        }

        [HttpPost("UploadFileToFolder")]
        public IActionResult UploadFileToFolder(FileUploadToSubFolderRequest request)
        {
            try
            {
                var _genericRecon = libMgr.UploadFileToFolder(request);

                return Ok(new { IsSuccess = true, Message = "SUCCESS", Data = _genericRecon});
                
            } catch(Exception ex)
            {
                return StatusCode(500, new { IsSuccess= false, Message = ex.Message});
            }
        }
    }
}
