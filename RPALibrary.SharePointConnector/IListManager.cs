﻿using RPALibrary.SharePointConnector.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPALibrary.SharePointConnector
{
   public interface IListManager
    {
        List<TaxResponse> GetTax(RequestList request);
    }
}
