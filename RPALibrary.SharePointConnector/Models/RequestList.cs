﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPALibrary.SharePointConnector.Models
{
    public class RequestList
    {
        public string List { get; set; }

        public string View { get; set; }

    }
}
