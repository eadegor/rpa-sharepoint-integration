using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RPALibrary.SharePointConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPALibrary.SharePointAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<AuthenticationManager>((s) => GetSPAuthManager());
            //services.AddScoped<ILibraryManager>((s) => GetSPLibManager());
            services.AddScoped<ILibraryManager, LibraryManager>();
            services.AddScoped<IListManager, ListManager>();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private AuthenticationManager GetSPAuthManager()

        {

            var siteUrl = Configuration["SiteUrl"];

            var clientId = Configuration["ClientId"];

            var clientSecret = Configuration["ClientSecret"];

            var tenant = Configuration["Tenant"];

            var resource = Configuration["Resource"];

            var grantType = Configuration["GrantType"];




           return new AuthenticationManager(siteUrl, grantType, resource, clientId, clientSecret, tenant);

           // return new LibraryManager(auth);

        }
    }
}
