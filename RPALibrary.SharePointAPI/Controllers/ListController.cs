﻿using Microsoft.AspNetCore.Mvc;
using RPALibrary.SharePointConnector;
using RPALibrary.SharePointConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RPALibrary.SharePointAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ListController : Controller
    {
        private readonly IListManager listMgr;


        public ListController(IListManager listMgr)
        {
            this.listMgr = listMgr;

        }

        [HttpPost("GetTax")]
        public IActionResult GetTax(RequestList request)
        {
            try
            {
                var _taxRecon = listMgr.GetTax(request);

                return Ok(new { IsSuccess = true, Message = "SUCCESS", Data = _taxRecon });

            }
            catch (Exception ex)
            {
                return StatusCode(500, new { IsSuccess = false, Message = ex.Message });
            }

        }

 



    }
}
